<div class="wrap">

	<div id="icon-options-general" class="icon32"></div>
	<h1><?php esc_attr_e( 'The Pango Plugin' ); ?></h1>

	<div id="poststuff">

		<div id="post-body" class="metabox-holder columns-2">

			<!-- main content -->
			<div id="post-body-content">

				<div class="meta-box-sortables ui-sortable">

					<div class="postbox">

						<div class="handlediv" title="Click to toggle"><br></div>
						<!-- Toggle -->

						<h2 class="hndle"><span><?php esc_attr_e( 'Your Site meta Data' ); ?></span>
						</h2>

						<div class="inside">
							<p><?php esc_attr_e( 'Your site was created by the pango development team on the date(here). You have our constant support and can contact us using the email form to the right or via email at (email address here). Further details specified by developer etc etc...'); ?></p>
						</div>
						<!-- .inside -->

					</div>
					<!-- .postbox -->

				</div>
				<!-- .meta-box-sortables .ui-sortable -->

			</div>
			<!-- post-body-content -->
			<!-- sidebar -->
			<div id="postbox-container-1" class="postbox-container">

				<div class="meta-box-sortables">

					<div class="postbox">

						<div class="handlediv" title="Click to toggle"><br></div>
						<!-- Toggle -->

						<h2 class="hndle"><span><?php esc_attr_e(
									'Contact Us'
								); ?></span></h2>

						<div class="inside">
							<form action="" method="post">
								Talk to us:
								<input type="textarea" name="contfrom" value="">
								<hr>
						    <input type="submit" value="Send Email" />
						    <input type="hidden" name="button_pressed" value="1" />
						</form>

						<?php

						if(isset($_POST['button_pressed']))
						{
						    $to      = 'reece@pango.world';
						    $subject = 'the subject';
						    $message = esc_attr($_REQUEST['contfrom']);
						    $headers = 'From: reece.e.chetty@gmail.com' . "\r\n" .//useremail account we set
						        'Reply-To: reece@pango.world' . "\r\n" .//our contact email
						        'X-Mailer: PHP/' . phpversion();

						    mail($to, $subject, $message, $headers);

						    echo $messgae;
							}

						?>
						</div>
						<!-- .inside -->

					</div>
					<!-- .postbox -->

				</div>
				<!-- .meta-box-sortables -->

			</div>
			<!-- #postbox-container-1 .postbox-container -->

		</div>
		<!-- #post-body .metabox-holder .columns-2 -->

		<br class="clear">
	</div>
	<!-- #poststuff -->

</div> <!-- .wrap -->
