<?php
/*
 *	Plugin Name: Pango WordPress Client Dashboard Plugin
 *	Plugin URI: http://wptreehouse.com/wptreehouse-badges-plugin//*repositry we need to store
 *	Description: Provides custom functionality for the WordPress admin environment brought to you by Pango.
 *	Version: 1.0
 *	Author: Reece Chetty
 *	Author URI: http://pango.world/
 *	License: GPL2
 *
*/
/*==============Create Plugin Menu================*/

function pango_client_options_menu(){
  add_options_page(
  'The Pango Plugin',
  'Pango',
  'manage_options',
  'pango_client_dashboard_plugin',
  'pango_client_dashboard_options_page'
  );
}
add_action('admin_menu','rec_pango_plugin_menu');

/*=============Create Plugin Page===================*/
function pango_client_dashboard_options_page(){
  if(!current_user_can('manage_options')){
    wp_die('You do not have sufficent privilages to access this page please contact Pango development/support team');
  }
  require('inc/options-page-wrapper.php');
}

/*=======Add Admin bar Icon and Create Pango menu===
 * Add a menu, with a dropdown to link that opens in a new window
 * Change the Menu title, the link title and and href link.
 */pango_client_admi

 function custom_adminbar_menu( $meta = TRUE ) {
    global $wp_admin_bar;
        if ( !is_user_logged_in() ) { return; }
        if ( !is_super_admin() || !is_admin_bar_showing() ) { return; }
    $wp_admin_bar->add_menu( array(
        'id' => 'pango_menu',
        'title' => __( '<img src="'.plugin_dir_url(__FILE__).'Pango_plain_white.png">' ),       /* set the menu name and direct to the pango logo */
        'href' => 'options-general.php?page=rec_pango-plugin') /*takes the user to the pango plugin settings page*/
    );
    $wp_admin_bar->add_menu( array(
        'parent' => 'pango_menu',
        'id'     => 'pango_links',
        'title' => __('Visit Us'),            /* Lets users know to vist the main pango page */
        'href' => 'http://pango.world',      /* Set the link to the pango website*/
        'meta'  => array( target => '_blank' ) )
    );
    $wp_admin_bar->add_menu( array(
        'parent' => 'pango_menu',
        'id'     => 'help_links',
        'title' => __('Support'),            /* Set the support lable */
        'href' => 'options-general.php?page=rec_pango-plugin',      /* Set the link to the support/ settings page */
        'meta'  => array( target => '_blank' ) )
    );
    /*Need to move this css to an external file*/
    echo '<style type="text/css">
          #wp-admin-bar-pango_menu img{
            height:inherit;
          }
          </style>';
 }
 add_action( 'admin_bar_menu', 'custom_adminbar_menu', 1 );

function remove_footer_admin(){
  echo 'Handcrafted Wordpress by <a href="http://pango.world" traget="blank">Pango</a> | Fueled by <a href="http://www.wordpress.org" target="_blank">WordPress</a> ';
}
add_filter('admin_footer_text','remove_footer_admin')
?>
