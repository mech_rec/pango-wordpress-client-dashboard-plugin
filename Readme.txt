=== Plugin Name ===
Contributors: Reece(wordpress.org signin id pennding)
Donate link: N/A
Tags: Pango, Rebrand, Customize, Advantage Learn Technologies, Development
Requires at least: 3.0.1
Tested up to: 4.5.3
Stable tag: 0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin was created by Reece Chetty as a deliverable for his internship at pango development branch. It allows for custom menus to be created to allow users to learn more about their choice of wp developer and to gain access to support.

== Description ==

This is a plugin developed with the idea of re-branding the wp-admin for the pango devison of Advantage Learn Technologies. The plugin changes multiple points of the WordPress admin section with the intent of conveying the custom wordpress development that goes into ever website created by Pango.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress
1. Use the Settings->Plugin Name screen to configure the plugin
1. (Make your instructions match the desired user flow for activating and installing your plugin. Include any steps that might be needed for explanatory purposes)


== Frequently Asked Questions ==

= What is Pango?

Come visit us and find out.

= What do we do? =

Handcrafted Wordpress!.

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png`
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==

= 0.1 =
* A change since the previous version.
* Another change.
== Upgrade Notice ==
=NA
== Arbitrary section ==

== A brief Markdown Example ==

Ordered list:

1. Insert a Pango logo as the main menu tag
2. Create a contact form for support
3. Creates and about site development page
4. Changes the footer of the site.

Here's a link to [WordPress](http://wordpress.org/ "Your favorite software") and one to [Markdown's Syntax Documentation][markdown syntax].
Titles are optional, naturally.

[markdown syntax]: http://daringfireball.net/projects/markdown/syntax
            "Markdown is what the parser uses to process much of the readme file"

Markdown uses email style notation for blockquotes and I've been told:
> Asterisks for *emphasis*. Double it up  for **strong**.

`<?php code(); // goes in backticks ?>`
